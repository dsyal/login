import { Component, OnInit } from '@angular/core';
import { IUser } from '../services/IUser';
import { services } from '../services/sevices';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from '../login/Login';
import { authService } from '../services/auth.service';
import { Token } from '../services/token';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  token: Token;
  model = new Login();
  message: string;
 
  constructor(private _services: services, private _router: Router, private _authservice: authService) { }

  ngOnInit() {
    this._services.CurrentMessage.subscribe(message => this.message=message)
  }

    onSubmit() {
      this._authservice.doLogin(this.model.username, this.model.password).subscribe((data) => this.token = data);
      console.log(this.token);
      if (this.token) {
        this._router.navigate(['dashboard']);
      }
      this._services.changeMessage(this.token.toString());
      console.log(this.token.toString());
      //else {
      //  alert("Invalid Credentials");
      //}
  }
  getUserId(): Token {
        return this.token;
    }

  
}

