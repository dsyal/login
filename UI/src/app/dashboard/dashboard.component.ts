import { Component, OnInit } from '@angular/core';
import { IUser } from '../services/IUser';
import { services } from '../services/sevices';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  users: IUser[];
  message: string;
  userid: number;

  constructor(private _services: services) { }

  ngOnInit() {
    this._services.getEmployee().subscribe((data) => this.users = data);
    this._services.CurrentMessage.subscribe(message => this.message = message)
    this.userid = + this.message;
    console.log(this.userid);
  }

}
