import { Component, OnInit } from '@angular/core';
import { services } from '../services/sevices';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from '../login/Login';
import { authService } from '../services/auth.service';
import { Token } from '../services/token';
import { Register } from '../register/register';
import { Department } from '../services/Department';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register: Register;
  model = new Register();
  data: any;
  constructor(private _services: services, private _router: Router, private _authservice: authService) { }

  ngOnInit() {
    //this._services.getDepartment().subscribe((data) => this.users = data)
    this.resetForm();

  }

  resetForm(form?: NgForm)
  {
    if (form != null) {
      form.reset();
      this._services.user = {
        password: '',
        name: '',
        designation_id: null,
        salary: null
      }
    }
  }

  onSubmit(form: NgForm) {
    this._services.PostEmployee(form.value)
      .subscribe(
    //  () => { console.log('Success') }
    //);
      (data: any) => {
      if (data.Succeeded == true)
        this.resetForm(form);
    }
      );
  }
}
