export interface IUser{
  id: number;
  name: string;
  password: string;
  designation_id: number;
  designation: string;
  salary: number;
}
