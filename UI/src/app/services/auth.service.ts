import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { IUser } from './IUser';
import { Observable } from 'rxjs/Observable';
import { Token } from '../services/token';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()

export class authService {  
  constructor(private _http: Http) { }

  doLogin(username: string, password: string) {
    return this._http.get("http://localhost:49724/api/login/" + username + "/" + password)
      .map(res => <Token>res.json());
  }

}
