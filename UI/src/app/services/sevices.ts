import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestMethod, RequestOptions } from '@angular/http';
import { IUser } from './IUser';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Register } from '../register/register';
import { Department } from '../services/Department';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'



@Injectable()

export class services {
  private messageSource = new BehaviorSubject<string>("default message");
  CurrentMessage = this.messageSource.asObservable();
  user: Register;
  constructor(private _http: Http) { }

  changeMessage(message: string) {
    this.messageSource.next(message);
  }

  getEmployee(): Observable<IUser[]> {
    return this._http.get("http://localhost:49724/api/users")
      .map((response: Response) => <IUser[]>response.json())
      .catch(this.handleError);
  }

  handleError(error: Response) {
    console.error(error);
    return Observable.throw(error);
  } 


  getDepartment(): Observable<Department> {
    return this._http.get("http://localhost:49724/api/users")
      .map((response: Response) => <Department>response.json())
      .catch(this.handleError);
  }
  //original
  //PostEmployee(register: Register) {
  //  console.log(register);  
  //  //let headers = new Headers({ 'Content-Type': 'application/json'});
  //  //let options = new RequestOptions({ headers: headers });

  //  let url = "http://localhost:49724/api/create";
  //  let headers = new Headers({ 'Content-Type': 'application/json; charset=UTF-8' });
  //  let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });

  //  return this._http.post(url, JSON.stringify(register), options)
  //    .map(res => res.json())
  //    //.map(res => this.extractData= res)
  //    .catch(this.handleError)
  //}



  //PostEmployee(register: Register) {
  //  let url = "http://localhost:49724/api/create";
  //  let body: string = JSON.stringify(register);
  //  //let headers = new Headers();
  //  let headers = new Headers({ 'Content-Type': 'application/json' });
  //  //let headers.append('Content-Type', 'application/json');
  //  let options = new RequestOptions({ headers: headers });

  //  return this._http.post(url, body, options)
  //    .map((res: Response) => {
  //      return res;
  //    })
  //    .catch((error: any) => {
  //      // Error on post request.
  //      return Observable.throw(error);
  //    });
  //} 

  //PostEmployee(register: Register): Observable<Register> {
  //  let url = "http://localhost:49724/api/create";
  //  let headers = new Headers({ 'Content-Type': 'application/json' });
  //  let options = new RequestOptions({ headers: headers });
  //  return this._http.post(url, register, options)
  //    .map(this.extractData)
  //    .catch(this.handleErrorObservable);
  //}
  //private extractData(res: Response) {
  //  let body = res.json();
  //  return body || {};
  //}
  //private handleErrorObservable(error: Response | any) {
  //  console.error(error.message || error);
  //  return Observable.throw(error.message || error);
  //}

  PostEmployee(register: Register){
    var body = JSON.stringify(register);
    var headerOptions = new Headers({'Content-Type': 'application/json'});
    var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
   return this._http.post('http://localhost:49724/api/create', body, requestOptions)
      .map(res => res.json())
      .catch(this.handleError)
  }

}
