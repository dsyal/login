﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Model;
using WebAPI;
using System.Data.Entity;

namespace API.Function
{
    public class employeefunction
    {
        private EmployeeContext db = new EmployeeContext();
        public IQueryable<employee> GetAllEmployee()
        {
            var data = from u in db.Users
                       join d in db.Designations on u.designation_id equals d.designation_id
                       select new employee
                       {
                           id = u.id,
                           name = u.name,
                           password = u.password,
                           salary = u.salary,
                           designation_id = u.designation_id,
                           designation = d.designation
                       };
            return data;
        }
        public IQueryable<employee> GetEmployee(int id)
        {
            var data = from u in db.Users
                       join d in db.Designations on u.designation_id equals d.designation_id
                       where u.id == id
                       select new employee
                       {
                           id = u.id,
                           name = u.name,
                           password = u.password,
                           salary = u.salary,
                           designation_id = u.designation_id,
                           designation = d.designation
                       };
            return data;
        }
        public IQueryable<Designation> GetDepartment()
        {
            var data = from d in db.Designations
                       select new Designation
                       {
                           designation_id = d.designation_id,
                           designation = d.designation
                       };
            return data;
        }
        public void createUser(Users user)
        {
            db.Users.Add(user);
            db.SaveChanges();
        }
        //internal void editUser(Users users)
        //{
        //    db.Entry(users).State = EntityState.Modified;
        //    db.SaveChanges();
        //}
        
        //internal void deleteUser(int id, Users users)
        //{
        //    db.Users.Remove(db.Users.SingleOrDefault(u => u.id == id));  
        //    db.SaveChanges();
        //}

        internal int getUserIdByLogin(string name, string password)
        {
            int userid = db.Users.Where(u => u.name == name && u.password == password)
                        .Select(z => z.id).SingleOrDefault();
            return userid;
        }

    }
}