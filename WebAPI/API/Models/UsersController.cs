﻿using API.Function;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Model;
using System.Web.Http.Cors;

namespace API.Models
{
    [EnableCorsAttribute("http://localhost:52800", "*","*")]
    public class UsersController : ApiController
    {
        private employeefunction db = new employeefunction();
        
        // GET: api/Users
        [HttpGet]
        [Route ("api/users")]
        public IQueryable<employee> GetUsers()
        {
            return db.GetAllEmployee();
        }

        [HttpGet]
        [Route("api/users/{id}")]
        public IQueryable<employee> GetUsers(int id)
        {
            return db.GetEmployee(id);
        }

        [HttpGet]
        [Route("api/department")]
        public IQueryable<Designation> GetDepartment()
        {
            return db.GetDepartment();
        }


        [HttpPost]
        [Route("api/create")]
        public HttpResponseMessage PostCreate([FromBody]Users user)
        {
            var employeefunc = new employeefunction();
            employeefunc.createUser(user);

            var msg = Request.CreateResponse(HttpStatusCode.Created);
            msg.Headers.Location = new Uri(Request.RequestUri + user.id.ToString());
            return msg;
        }

        //[HttpPost]
        //[Route("api/user/edit/{id}")]
        //public IHttpActionResult Edit(int id, Users user)
        //{
        //    if(id!= user.id)
        //    {
        //        return BadRequest();
        //    }
        //    var employeefunc = new employeefunction();
        //    employeefunc.editUser(user);
        //    return Ok();
        //}

        //[HttpDelete]
        //[Route("api/user/delete/{id}")]
        //public IHttpActionResult Delete(int id, Users user)
        //{
        //    if (id != user.id)
        //    {
        //        return BadRequest();
        //    }
        //    var employeefunc = new employeefunction();
        //    employeefunc.deleteUser(id, user);
        //    return Ok();
        //}

        [HttpGet]
        [Route("api/login/{name}/{password}")]
        public IHttpActionResult Login(string name, string password)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(password))
            {
                return BadRequest("Please enter valid credentials");
            }
            else
            {
                var employeefunc = new employeefunction();
                int userid = employeefunc.getUserIdByLogin(name, password);
                if (userid <= 0)
                {
                    return BadRequest("Please enter valid credentials");
                }
                return Ok(userid);
            }
        }
    }
}   