namespace API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Designations",
                c => new
                    {
                        designation_id = c.Int(nullable: false, identity: true),
                        designation = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.designation_id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        password = c.String(nullable: false),
                        designation_id = c.Int(nullable: false),
                        salary = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Designations", t => t.designation_id, cascadeDelete: true)
                .Index(t => t.designation_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "designation_id", "dbo.Designations");
            DropIndex("dbo.Users", new[] { "designation_id" });
            DropTable("dbo.Users");
            DropTable("dbo.Designations");
        }
    }
}
