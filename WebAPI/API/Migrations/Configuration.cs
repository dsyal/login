namespace API.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebAPI.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<EmployeeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(EmployeeContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            
            var users = new List<Users>
            {
                new Users() {name="ram", password="welcome123",salary=5000,designation_id=2},
                new Users() {name="kiran", password="welcome123",salary=12000,designation_id=2},
                new Users() {name="rajesh", password="welcome123",salary=10000,designation_id=3}
            };
            users.ForEach(u => context.Users.Add(u));
            context.SaveChanges();

            //var designation = new List<Designation>
            //{
            //    new Designation() {designation="software engineer"},
            //    new Designation() {designation="SSE"},
            //    new Designation() {designation="TL"}
            //};
            //designation.ForEach(d => context.Designations.Add(d));
            //context.SaveChanges();
        }
    }
}
