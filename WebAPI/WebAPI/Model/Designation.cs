﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Model
{
    public class Designation
    {   
        [Key]
        public int designation_id { get; set; }
        [Required]
        public string designation { get; set; }

        public ICollection<Users> users { get; set; }
    }
}
