﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Model
{
    public class employee
    {
        
        public int id { get; set; }
        public string name { get; set; }
        public string password { get; set; }
        public int designation_id { get; set; }
        public int salary { get; set; }
        public string designation { get; set; }

    }
}
