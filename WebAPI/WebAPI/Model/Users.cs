﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Model
{
    public class Users
    {   
        [Key]
        public int id { get; set; }
        [Required]
        public string name { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public int designation_id { get; set; }
        [Required]
        public int salary { get; set; }

        public Designation designations { get; set; }


    }
}
